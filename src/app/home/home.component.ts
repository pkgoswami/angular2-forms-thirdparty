import {Component} from '@angular/core';

@Component({
  selector: 'home',
  styleUrls: ['./home.component.css'],
  templateUrl: './home.component.html'
})
export class HomeComponent {
	//startDate = new Date('Jan 1 2017');
	startDate: Date; // Today's Date default
	minDate = new Date('Nov 22 2016');
	startTime : Date; // Current Time
	//startTime = new Date('Nov 22 2016 11:00 AM');
	postRating = 5;

	hover(value){
		console.log("hover: " + value);
	}
	
	leave(value){
		console.log("leave: " + value);
	}
}
